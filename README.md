# NodeJS Development Environment
This VM is based on Ubuntu 16.04 and NodeJS 4.6. Tools installed:

* NodeJS 4.6
* Cloud9 Editor
* Git
* Build-essential (required to build NodeJS Add ons)

## Requires
* VirtualBox 5.1
* Vagrant 1.8.5

## Usage
After installing VirtualBox and Vagrant, modify the ```Vagrantfile``` and assign the RAM that you want (default is 3 GB). By default,
the image only opens port 8081 that is mapped to port 8080 within the VM, open as many ports as you need. Finally, execute the
following command.

```sh
vagrant up
```

Within the VM, in the ```/vagrant_data``` folder you will find the ```install.sh``` file that can be used to install Docker compose.